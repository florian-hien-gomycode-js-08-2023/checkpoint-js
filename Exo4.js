// Dominante positive
// Un tableau est à dominante positive s'il contient strictement plus de valeurs positives uniques que de valeurs négatives uniques . Écrivez une fonction qui renvoie si un tableau est à dominante positive . true

// Exemples
// isPositiveDominant([1, 1, 1, 1, -3, -4]) ➞ false
// // There is only 1 unique positive value (1).
// // There are 2 unique negative values (-3, -4).

// isPositiveDominant([5, 99, 832, -3, -4]) ➞ true

// isPositiveDominant([5, 0]) ➞ true

// isPositiveDominant([0, -4, -1]) ➞ false
// Remarques
// 0ne compte ni comme une valeur positive ni comme une valeur négative.


function isPositiveDominant(a) {
    let mySet = new Set(a)
    var neg = 0
    var pos = 0
    for (const nbr of mySet.values()) {
        if (nbr > 0) {
            pos++
        } else if (nbr < 0) {
            neg++
        }
    }
    return pos > neg 
}

console.log(isPositiveDominant([9, 4, -1]));

