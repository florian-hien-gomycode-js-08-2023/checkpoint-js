// Dissocier les données dans un objet
// Vous vous êtes porté volontaire pour aider à enseigner dans une école maternelle de votre région ! Vous avez reçu un tableau de tous les élèves et des données importantes les concernant, regroupées par leur professeur. Créez une fonction qui dissociera chaque élève afin que vous puissiez consulter ses détails individuellement.

// Exemple
// ungroupStudents([{
//   teacher: "Ms. Car",
//   data: [{
//      name: "James",
//      emergenceNumber: "617-771-1082",
//   }, {
//      name: "Alice",
//      alergies: ["nuts", "carrots"],
//   }],
// }, {
//   teacher: "Mr. Lamb",
//   data: [{
//     name: "Aaron",
//     age: 3
//   }]
// }]) ➞ [{
//   teacher: "Ms. Car",
//   name: "James",
//   emergencyNumber: "617-771-1082",
// }, {
//   teacher: "Ms. Car",
//   name: "Alice",
//   alergies: ["nuts", "carrots"],
// }, {
//   teacher: "Mr. Lamb",
//   name: "Aaron",
//   age: 3,
// }]


function ungroupStudents(students) {
    let tab=[]
    for(let item of students) {
      for(let el of item.data) {
        tab.push({teacher:item.teacher,...el })
      }
    }
    return tab
}

