// Nombres dans des chaînes
// Créez une fonction qui prend un tableau de chaînes et renvoie un tableau contenant uniquement les chaînes contenant des nombres. S'il n'y a aucune chaîne contenant des nombres, renvoie un tableau vide.

// Exemples
// numInStr(["1a", "a", "2b", "b"]) ➞ ["1a", "2b"]

// numInStr(["abc", "abc10"]) ➞ ["abc10"]

// numInStr(["abc", "ab10c", "a10bc", "bcd"]) ➞ ["ab10c", "a10bc"]

// numInStr(["this is a test", "test1"]) ➞ ["test1"]
// Remarques
// Les chaînes peuvent contenir des espaces blancs ou tout type de caractères.
// Bonus : essayez de résoudre ce problème sans RegEx.

function numInStr(arr) {
  const result = [];

  for (const str of arr) {
    for (const char of str) {
      if (!isNaN(char)  && char!= ' ') {
        result.push(str);
        break;
      }
    }
  }

  return result
}

console.log(numInStr(["abc", "ab10c", "a10bc", "bcd"]));
