// 24-Hour Time
// Write a function that receives the time in 12-hour AM/PM format and returns a string representation of the time in military (24-hour) format.

// Examples
// convertTime(“07:05:45PM”) ➞ “19:05:45”

// convertTime(“12:40:22AM”) ➞ “00:40:22”

// convertTime(“12:45:54PM”) ➞ “12:45:54”
// Notes
// Midnight is 12:00:00AM on a 12-hour clock, and 00:00:00 on a 24-hour clock.
// Noon is 12:00:00PM on a 12-hour clock, and 12:00:00 on a 24-hour clock.

// function convertTime(str) {
//   if (str.includes("PM")) {
//     str = str.substr(0, str.length - 2).split(":")
//     if (str[0]== "12") {
//         return str.join(":")
//     } else {
//         str[0]= (parseInt(str[0]) + 12).toString()
//         return str.join(":")
//     }
//   } else if (str.includes("AM")) {
//     str = str.substr(0, str.length - 2).split(":")
//     if (str[0]== "12") {
//         str[0]= "00"
//         return str.join(":")
//     } else {
//         return  str.join(":")
//     }
//   }
// }

function convertTime(str) {
  const isMorning = str.includes("AM");
  str = str.substr(0, str.length - 2).split(":");
  if (isMorning && str[0] === "12"){
    str[0]= "00"
  } else if (isMorning == false && str[0] !== "12"){
    str[0]= (parseInt(str[0]) + 12).toString()
  }
  
  return str.join(":")
}

console.log(convertTime("12:45:54PM"));
