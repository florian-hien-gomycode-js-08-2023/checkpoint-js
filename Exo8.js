// Convertir des minutes en secondes
// Écrivez une fonction qui prend un entier minuteset le convertit en secondes.

// Exemples
// convert(5) ➞ 300

// convert(3) ➞ 180

// convert(2) ➞ 120
// Remarques
// N'oubliez pas returnle résultat.
// Si vous êtes bloqué sur un défi, trouvez de l'aide dans l' onglet Ressources .
// Si vous êtes vraiment bloqué, débloquez des solutions dans l' onglet Solutions .



function convert(minutes) {
    return minutes*60 
}