// Does the Triangle Fit into the Triangular Hole?
// Create a function that takes the dimensions of two triangles (as arrays) and checks if the first triangle fits into the second one.

// Examples
// doesTriangleFit([1, 1, 1], [1, 1, 1]) ➞ true

// doesTriangleFit([1, 1, 1], [2, 2, 2]) ➞ true

// doesTriangleFit([1, 2, 3], [1, 2, 2]) ➞ false

// doesTriangleFit([1, 2, 4], [1, 2, 6]) ➞ false
// Notes
// Triangle fits if it has the same or smaller size as the hole.
// The function should return false if the triangle with that dimensions is not possible.

function doesTriangleFit(brick, hole) {
  const isTriangle = (t) => t[0] + t[1] > t[2];

  if (!isTriangle(brick) || !isTriangle(hole)) return false;

  for (let i = 0; i < 3; i++) {
    if (brick[i] > hole[i]) return false;
  }
  return true;
}

console.log(doesTriangleFit([1, 1, 1], [2, 2, 2]));
