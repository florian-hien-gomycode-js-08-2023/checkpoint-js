// Numéros pannumériques
// Un numéro pandigital contient tous les chiffres (0 à 9) au moins une fois. Écrivez une fonction qui prend un entier, renvoyant truesi l'entier est pannumérique, et falsesinon.

// Exemples
// isPandigital(98140723568910) ➞ true

// isPandigital(90864523148909) ➞ false
// // 7 is missing.

// Remarques
// Pensez aux propriétés d'un numéro pannumérique lorsque tous les doublons sont supprimés.

// function isPandigital(num) {
//     num = num.toString()
//   if (
//     num.includes(0) &&
//     num.includes(1) &&
//     num.includes(2) &&
//     num.includes(3) &&
//     num.includes(4) &&
//     num.includes(5) &&
//     num.includes(6) &&
//     num.includes(7) &&
//     num.includes(8) &&
//     num.includes(9)
//   ) {
//     return true;
//   } else {
//     return false;
//   }
// }


function isPandigital(num) {
  var mySet = new Set(Array.from(num.toString()))
  for (let el of mySet) {
    console.log(el);
  }
  return mySet.size == 10
}

console.log(isPandigital(90864523148909));
