// Convertir un nombre en base 2
// Créez une fonction qui renvoie une représentation en base 2 (binaire) d'un nombre de chaîne en base 10 (décimal). La conversion est simple : ((2) signifie base-2 et (10) signifie base-10) 010101001(2) = 1 + 8 + 32 + 128.

// En allant de droite à gauche, la valeur du bit le plus à droite est 1, maintenant à partir de là, chaque bit vers la gauche sera x2 la valeur, la valeur d'un nombre binaire de 8 bits est (256, 128, 64, 32, 16, 8 , 4, 2, 1).

// Exemples
// binary(1) ➞ "1"
// 1*1 = 1

// binary(5) ➞ "101"
// 1*1 + 1*4 = 5

// binary(10) ➞ "1010"
// 1*2 + 1*8 = 10
// Remarques
// Les nombres seront toujours inférieurs à 1024 (sans compter 1024).
// L' &&opérateur pourrait être utile.
// Les chaînes iront toujours jusqu'à la longueur à laquelle la valeur du bit le plus à gauche devient supérieure au nombre dans decimal.
// Si une conversion binaire pour 0 est tentée, renvoyez "0".

function binary(decimal) {
    var tab = []
    var quotient = decimal
    if (decimal === 0) return '0'
    while (quotient !== 0) {
        tab.unshift(quotient)
        quotient = Number.isInteger(quotient/2) ? quotient/2 : Math.floor(quotient/2)
    }
    const result = tab.map(el => Math.round(el%2))
    return result.join('')
}


console.log(binary(10));
